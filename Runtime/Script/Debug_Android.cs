﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public class Debug_Android : MonoBehaviour
{
    public Text m_debugText;
 
    void Update()
    {
        if (m_debugText == null)
            return;
        Input.gyro.enabled = true;
        Input.compass.enabled = true;

        string toDisplay = "";
        toDisplay += "Gyro, Is Availaible:" + SystemInfo.supportsGyroscope;
        toDisplay += "\nGyro, Atti:" + Input.gyro.attitude;
        toDisplay += "\nGyro, Is Availaible:" + SystemInfo.supportsGyroscope;
        toDisplay += "\nAccelerometer, Value:" + Input.acceleration;

        toDisplay += "\nCompass, Accuracy :" + Input.compass.headingAccuracy;
        toDisplay += "\nCompass, Raw :" + Input.compass.rawVector;
        toDisplay += "\nOrientation :" + Input.deviceOrientation;

        m_debugText.text = toDisplay;
        

    }
}
